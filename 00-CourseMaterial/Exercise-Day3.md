## Exercise: Three Apps

Compare the three ToDo-applications at the following urls:

- [https://spa-demos.herokuapp.com/todo1/](https://spa-demos.herokuapp.com/todo1/)
- [https://spa-demos.herokuapp.com/todo2/](https://spa-demos.herokuapp.com/todo2/)
- [https://spa-demos.herokuapp.com/todo3/](https://spa-demos.herokuapp.com/todo3/)

What can you find out about their technical implementation?  
Write down all the differences.

(The source code for the three examples is in `01-Intro/00-ToDo-Apps`)

For the curious, have a look at: https://spa-demos-ssr.herokuapp.com/ 
