# Dependency Management

Install all build dependencies and front-end assets of the project:

    npm install

# Start the application

    npm start

Then go to `http://localhost:5678`

# Create the production build (linting, asset processing)

    npm run build
    npm run serve:prod

Then go to `http://localhost:5679`.  
Inspect the result in `dist`.


# Analyze the bundles

The webpack build generates a report into `build/bundle-report.html`.

Another way is to run the command `npx source-map-explorer 'dist/app/**/*.js'`.


# Run the Unit Tests

    npm run test

or:

    npm run test:jest

or:

    npm run test:karma
    
    
# Run the End-to-End Tests (Cypress)

With the development UI:

    npm build
    npm serve:prod
    npm run cypress:dev

Headless:

    npm build
    npm serve:prod
    npm run cypress
    
    

# Deploy on GitHub Pages

    npm run deploy

For information about GitHub Pages see: https://help.github.com/articles/user-organization-and-project-pages/
