/* eslint-env node */
module.exports = function (api) {
    const isTest = api.env('test');

    const presets = [
        [
            '@babel/env',
            {
                targets: {
                    ie: 11,
                    edge: '17',
                    firefox: '60',
                    chrome: '80',
                    safari: '11.1',
                },
                modules: isTest ? 'commonjs' : false,
            },
        ],
        '@babel/preset-typescript',
    ];

    const plugins = ['@babel/plugin-proposal-class-properties'];

    return { presets, plugins };
};
