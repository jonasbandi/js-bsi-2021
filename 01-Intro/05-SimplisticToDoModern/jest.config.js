/* eslint-env node */

module.exports = {
    roots: ['src/js'],
    testEnvironment: 'jsdom',
    reporters: ['default', ['jest-junit', { outputDirectory: './build-info' }]],
};
