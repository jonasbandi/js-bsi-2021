console.log('Running example ...');

function add(first, second) {
  return first + second;
}

function defineFirstArg(first, func){
  return function(second){
    return func(first, second);
  }
}
// A more compact form:
// const defineFirstArg = (first, func) => (second) => func(first, second);

const addOne = defineFirstArg(1, add);
const result = addOne(22);
console.log(result);


function wrapWithLog(func){
  return function (...args) {
    console.log('Arguments', args);
    const returnValue = func(...args);
    console.log('Result', returnValue);
    return returnValue;
  }
}

const loggingAdd = wrapWithLog(add);

const result2 = loggingAdd(41, 1);
console.log(result2);





//////////////
// Exercise
//////////////
function createRangeValidator(lowerBound, upperBound){
  return function (num) {
    return true;
  }
}

const checkSmallRange = createRangeValidator(5, 20);
const checkBigRange = createRangeValidator(900, 1000);

console.log('Small 1', checkSmallRange(11)); // should be 'true'
console.log('Small 2', checkSmallRange(33)); // should be 'false'
console.log('Small 3', checkSmallRange(3)); // should be 'false'
console.log('Big 1', checkBigRange(11)); // should be 'false'
console.log('Big 2', checkBigRange(999)); // should be 'true'
console.log('Big 3', checkBigRange(3000));  // should be 'false'


//////////////
// Exercise
//////////////
function fibonacci(n) {
  if (n < 2)
    return 1;
  else
    return fibonacci(n - 2) + fibonacci(n - 1);
}

// Task: Implement  the `memo` function,
// so that the second calculation of the Fibonacci number of 42 below is much faster
function memo(func){
  // TODO
  return func;
}


const memoFib = memo(fibonacci);
console.log('Calculating ...');
const fibResult = memoFib(40);
console.log('Calculated', fibResult);
console.log('Calculating once more ...');
const fibResult2 = memoFib(40); // This should be much faster!
console.log('Calculated once more', fibResult2);



//////////////
// Exercise
//////////////
// Task: Write a `compose` function that allows to apply a sequence of functions to a value
const scream = str => str.toUpperCase();
const exclaim = str => `${str}!`;
const repeat = str => `${str} ${str}`;

const processedValue = repeat(exclaim(scream('I love JavaScript')));
console.log('Processed Value:', processedValue);

function compose(...funcs){
  return () => 'TODO';
}

const processedValue2 = compose(scream, exclaim, repeat)('I love JavaScript');
console.log('Should be the same value:', processedValue2);
