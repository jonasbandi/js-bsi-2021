/* eslint-env node */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/app/app.js',
  output: {
    filename: 'bundle.[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      { test: /\.ts$/, use: 'ts-loader'},
      {	test: /\.css$/,	use: [ {loader: MiniCssExtractPlugin.loader}, 'css-loader'] }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({template: 'src/index.html', scriptLoading: 'blocking' }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    })
  ]
};
