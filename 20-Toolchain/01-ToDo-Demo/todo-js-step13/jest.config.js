/* eslint-env node */
module.exports = {
  roots: ['src'],
  testEnvironment: 'jsdom',
  preset: 'ts-jest',
  reporters: ['default', ['jest-junit', { outputDirectory: './build-info' }]]
};

