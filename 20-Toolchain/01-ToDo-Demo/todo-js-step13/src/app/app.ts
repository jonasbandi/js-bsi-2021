import '../css/index.css';
import 'core-js/stable/promise'; // needed for IE
import $ from 'jquery';
import { applyLayout } from './layout';
import { ToDoModel } from './model';

console.log('Starting app ... ');

applyLayout();

const $app = $('#app');
const model = new ToDoModel();

$(model).on('modelchange', () => {
  $('#todo-text').val(model.newToDo.title);
  renderToDoList($('#todo-list'));
});

function renderForm() {
  const $input = $('<input>', {
    id: 'todo-text',
    placeholder: 'What do you want to do?'
  }).on('blur', () => {
    model.updateNewToDoModel($input.val().toString());
  });

  $('<form>')
    .addClass('new-todo')
    .append(
      $input,
      $('<button>', { id: 'add-button' })
        .addClass('add-button')
        .text('+')
    )
    .on('submit', event => {
      event.preventDefault();
      model.updateNewToDoModel($input.val().toString());
      model.addToDo();
    })
    .appendTo($app);
}

function renderToDoList($todoList) {
  $todoList.html('');

  for (const todo of model.toDoList) {
    $('<li>')
      .appendTo($todoList)
      .text(todo.title)
      .append(
        $('<button>')
          .text('X')
          .on('click', function() {
            import('./removeItem').then(m => {
              m.removeItem(model, todo);
            });
          })
      );
  }
}

function renderApp() {
  $app.html('');
  renderForm();
  const $todoListContainer = $('<div>')
    .addClass('todo-list-container')
    .appendTo($app);
  const $todoList = $('<ul>')
    .prop('id', 'todo-list')
    .addClass('todo-list')
    .appendTo($todoListContainer);
  renderToDoList($todoList);
}

renderApp();
