import $ from 'jquery';

export class ToDo {
  title: string;
  constructor(title: string) {
    this.title = title;
  }
}

export class ToDoModel {
  newToDo = new ToDo('');
  toDoList: ToDo[] = [new ToDo('Learn JavaScript'), new ToDo('Learn React')];

  $vm = $(this);

  updateNewToDoModel(title: string) {
    if (this.newToDo.title !== title) {
      this.newToDo.title = capitalize(title);
      this.notifyModelChange();
    }
  }
  addToDo() {
    this.toDoList.push(this.newToDo);
    this.newToDo = new ToDo('');
    this.notifyModelChange();
  }
  removeToDo(toDo: ToDo) {
    this.toDoList = this.toDoList.filter(t => t !== toDo);
    this.notifyModelChange();
  }
  notifyModelChange() {
    this.$vm.trigger('modelchange');
  }
}

function capitalize(value: string) {
  return value.charAt(0).toUpperCase() + value.slice(1);
}
