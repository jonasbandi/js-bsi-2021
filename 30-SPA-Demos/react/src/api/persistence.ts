import axios from 'axios';
import { ToDo, ToDoPostResponse, ToDosGetResponse } from '../api/types';

const API_URL = 'http://localhost:3456/todos';

export async function loadToDos(completed = 0) {
  const serverResponse = await axios.get<ToDosGetResponse>(API_URL, { params: { completed } });
  return serverResponse.data.data;
}

export async function saveToDo(toDo: ToDo) {
  try {
    const serverResponse = await axios.post<ToDoPostResponse>(API_URL, toDo);
    return serverResponse.data.data;
  } catch {
    alert('Something went terribly wrong!');
    window.location.reload();
  }
}

export async function updateToDo(toDo: ToDo) {
  try {
    await axios.put(`${API_URL}/${toDo.id}`, toDo);
  } catch (error) {
    console.log(error);
  }
}

export async function deleteToDo(toDo: ToDo) {
  try {
    await axios.delete(`${API_URL}/${toDo.id}`);
  } catch (error) {
    console.log(error);
  }
}
