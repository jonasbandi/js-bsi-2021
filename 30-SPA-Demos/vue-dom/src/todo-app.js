var todoApp = new Vue({
  el: '#todoapp',
  data: {
    newToDoTitle: '',
    // todos: [{title: 'Learn Vue'}, {title: 'Learn React'}, {title: 'Learn Angular'},]
    todos: undefined
  },
  computed: {
    todoCount: function () {
      return this.todos.length;
    }
  },
  methods: {
    addToDo: function () {
      // NOTE: id-property must be set so that Vue tracks changes on that property
      const newToDo = {id: undefined, title: this.newToDoTitle, completed: false};
      this.todos.push(newToDo);
      this.newToDoTitle = '';
      $todos.persistence.post(newToDo)
        .then(persistedToDo => {
          newToDo.id = persistedToDo.id;
        });
    },
    removeToDo: function (todo) {
      const position = this.todos.indexOf(todo);
      if (position >= 0) {
        this.todos.splice(position, 1)
      }
      $todos.persistence.delete(todo);
    }
  },
  created: function () {
    $todos.persistence.get()
      .then(todos => {
        this.todos = todos;
      })
  }

});
