(function () {
  'use strict';

  const API_URL = 'http://localhost:3456/todos';

  window.$todos = window.$todos || {};

  window.$todos.persistence = {
    get,
    post,
    delete: del
  };

  function get() {
    return axios.get(API_URL)
      .then(response => {
        return response.data.data;
      })
      .catch((error) => console.log(error));
  }

  function post(todo) {
    return axios.post(API_URL, todo)
      .then(response => {
        return response.data.data;
      })
      .catch((error) => console.log(error));
  }

  function put(todo) {
    return axios.put(`${API_URL}/${todo.id}`, todo)
      .catch((error) => console.log(error));
  }

  function del(todo) {
    return axios.delete(`${API_URL}/${todo.id}`)
      .catch((error) => console.log(error));
  }
})();
