import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import ToDoScreen from './views/todo-screen/ToDoScreen.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'ToDo',
    component: ToDoScreen,
  },
  {
    path: '/done',
    name: 'Done',
    // route level code-splitting
    // this generates a separate chunk (done.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "done" */ './views/done-screen/DoneScreen.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: routes,
});

export default router;
